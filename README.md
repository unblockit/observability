# UnblockIT

## What is monitoring?
O monitoramento é uma forma de se manter atualizado sobre a integridade e o desempenho dos serviços. O serviço de monitoramento integrado do `prometheus` ajuda a consolidar as etapas de observalidade dos projetos.

## Getting Started
Para esse guide a montagem do ambiente foi utilizado o Docker Compose para demostrar a parte do service discovery.

### Usando Docker
Todos os serviços do Prometheus / Grafana estão disponíveis como imagens Docker no Quay.io ou Docker Hub .

## Executar Stack Monitoramento
Para executar toda stack de monitoramento no `docker` execute o comando abaixo. 

> Running Project

Na raiz do projeto execute: 

```shell script
docker-compose up # use -d for background running
```

> Listing all service

```shell script
docker-compose ps
```

### Prometheus
O comando acima inicia o `prometheus` com as configuração de amostra para o unblock-it, com as `rules` e `service discovery` habilitados e a expõe na porta `9090`.

Caso precise alterar as configurações, verificar `prometheus/prometheus.yml` e `prometheus/rules.yml`.

#### Alertmanager
O comando acima inicia `alertmanager` com as configuração de amostra para o unblock-it, com canal do `webhook` para `GitLab` e a expõe na porta `9093`.

Caso precise alterar as configurações, verificar `alertmanager/alertmanager.yml`.


#### Grafana
O comando acima inicia `grafana` com as configuração de amostra para o unblock-it e a expõe na porta `3000`.

Caso precise alterar as configurações, verificar o diretório `grafana`.

Alguns dashboards já foram importados para facilitar a demonstração:
* [JVM Micrometer](https://grafana.com/grafana/dashboards/4701)
* [Prometheus .NET](https://grafana.com/grafana/dashboards/10427)

**Para adicionar mais dashboards, basta verificar o diretorio `grafana/provisioning/dashboards`**

## Guides
Os guias a seguir ilustram como usar alguns recursos concretamente:

* [Grafana](https://grafana.com/docs/grafana/latest/installation/configure-docker/)
* [Prometheus](https://prometheus.io/docs/prometheus/latest/installation/)
* [Alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/)
* [Alerting Rules](https://prometheus.io/docs/prometheus/latest/configuration/alerting_rules/)
* [Defining Recording Rules](https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/#:~:text=Prometheus%20supports%20two%20types%20of,field%20in%20the%20Prometheus%20configuration.)